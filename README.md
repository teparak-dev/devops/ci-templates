# CI Templates
    VIEWER: https://teparak-dev.gitlab.io/devops/ci-templates

# Variables
## Per Project
IMAGE_REGISTRY_IMAGE


## Global Variable

### Docker
IMAGE_REGISTRY_USERNAME
IMAGE_REGISTRY_PASSWORD
IMAGE_REGISTRY_URL


# Kubernetes Deployment
KUBE_CONFIG
KUBE_NAMESPACE
HELM_CHART_VERSION
HELM_CHART_REPO