ARG USERNAME=swadm
ARG GROUP_ID=1001
ARG USER_ID=1001
ARG PYTHON_VERSION="3.11"
ARG POETRY_VERSION="1.3.1"
ARG PYTHON_APT_PACKAGE_NAME="python$PYTHON_VERSION"

FROM python:${PYTHON_VERSION}-buster as builder

ARG POETRY_VERSION
ARG PYTHON_VERSION
ARG PYTHON_APT_PACKAGE_NAME

ENV HOME=/root

RUN apt-get update && apt-get -y upgrade && \
    apt-get install --no-install-recommends -y git curl && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get purge --auto-remove && \
    apt-get clean

WORKDIR /code

ENV PYTHONUNBUFFERED=1 \
    PYTHONFAULTHANDLER=1 \
    PYTHONHASHSEED=random \
    PYTHONDONTWRITEBYTECODE=1 \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    POETRY_NO_INTERACTION=1 \
    POETRY_NO_ANSI=true \
    POETRY_VIRTUALENVS_IN_PROJECT=true \
    POETRY_VIRTUALENVS_CREATE=true

ENV PATH="$HOME/.local/bin:$PATH"
RUN curl -sSL https://install.python-poetry.org | python3 -

COPY pyproject.toml poetry.lock ./
RUN poetry check && \
    poetry lock --no-update && \
    poetry install --without dev

FROM ubuntu:focal AS final

ARG USERNAME
ARG USER_ID
ARG GROUP_ID
ARG PYTHON_VERSION
ARG PYTHON_APT_PACKAGE_NAME

# Set up a non-root user
ENV HOME=/home/$USERNAME
ENV LANG=C.UTF-8
ENV PATH="$HOME/.local/bin:/usr/bin:/usr/sbin:$PATH"
ENV PYTHONPATH="/usr/lib/$PYTHON_APT_PACKAGE_NAME/site-packages:$PYTHONPATH"
ENV PYTHONUNBUFFERED=1 \
    PYTHONFAULTHANDLER=1 \
    PYTHONHASHSEED=random \
    PYTHONDONTWRITEBYTECODE=1 \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100

RUN apt-get update && apt-get -y --no-install-recommends install software-properties-common && \
    add-apt-repository -y ppa:deadsnakes/ppa && apt-get remove --purge -y software-properties-common && \
    apt-get update && apt-get -y --no-install-recommends install $PYTHON_APT_PACKAGE_NAME $PYTHON_APT_PACKAGE_NAME-dev python3-pip && \
    # Python 3.8 May be installed by some other lib. Ensure removal
    apt-get remove --purge -yf python3.8 libasn1-8-heimdal && \
    apt-get purge -y && apt-get autoclean -y && apt-get autoremove -y && \
    ln -sf /usr/bin/$PYTHON_APT_PACKAGE_NAME /usr/bin/python3 && \
    ln -sf /usr/bin/$PYTHON_APT_PACKAGE_NAME /usr/bin/python && \
    echo "$(which python3) -> $(python3 --version)" && \
    echo "$(which python) -> $(python --version)"

COPY --from=builder --chown=$USER_ID:$GROUP_ID /usr/local/lib/ /usr/local/lib/
COPY --from=builder --chown=$USER_ID:$GROUP_ID /code/.venv/lib/$PYTHON_APT_PACKAGE_NAME/site-packages/ /usr/lib/$PYTHON_APT_PACKAGE_NAME/site-packages/
#COPY --from=builder --chown=$USER_ID:$GROUP_ID /code/.venv/bin/ /usr/bin/


WORKDIR /code
ADD . .

# Call collectstatic (customize the following line with the minimal environment variables needed for manage.py to run):
RUN DATABASE_URL=" " python3 manage.py collectstatic --noinput

# Make sure file permissions are 644 and dir permissions are 755
RUN addgroup --gid $GROUP_ID $USERNAME &&  \
    adduser --disabled-password --uid $USER_ID --gid $GROUP_ID $USERNAME && \
    chown -R $USERNAME:$USERNAME . && \
    chmod -R u+rwX,go+rX,go-w .
USER $USERNAME

#RUN find /usr/bin/ -type f -name *python*
#RUN python -c ""

ENTRYPOINT ["/code/entrypoint.sh"]

STOPSIGNAL SIGTERM
ENV HTTP_PORT 8000
EXPOSE $HTTP_PORT

ENV WORKER_SPAWN_COUNT 8
ENV HTTP_TIMEOUT  600
ENV HTTP_KEEPALIVE 30

ARG GIT_COMMIT_SHORT
ENV GIT_COMMIT_SHORT $GIT_COMMIT_SHORT

ENTRYPOINT ["/code/entrypoint.sh"]
CMD python -m gunicorn \
    core.wsgi:application \
    --graceful-timeout 5 \
    --config core/geventlet-config.py \
    --keep-alive=$HTTP_KEEPALIVE \
    --logger-class core.logger.CustomGunicornLogger \
    --worker-class=gevent \
    --workers=$WORKER_SPAWN_COUNT \
    --access-logfile '-' \
    --bind :$HTTP_PORT \
    --timeout $HTTP_TIMEOUT \
    --preload
